class NavDrawer {
    constructor(toggleId, constraintId, mainContentId){
        this.transitionTime = 300;
        this.toggleElement = document.getElementById(toggleId);
        this.siteConstraint = document.getElementById(constraintId);
        this.mainContent = document.getElementById(mainContentId);
        this.overlay;
        this.navOpen = this.siteConstraint.classList.contains('nav-open');
        this.canToggle = true;
        this.init();
    }

    setCanToggle(){
        this.canToggle = false;
        setTimeout(()=>{
            this.canToggle = true;
        }, this.transitionTime);
    }

    createOverlay(){
        this.overlay = document.createElement('div');
        this.overlay.className = "dui-mobile-nav-overlay";
        if (!this.navOpen){
            this.overlay.classList.add('hidden');
        }

        this.mainContent.append(this.overlay);
    }

    toggle = () => {
        if(!this.canToggle) return 'cant toggle';
    
        this.setCanToggle();

        if (this.navOpen){
            this.navOpen = !this.navOpen;
            this.siteConstraint.classList.remove('nav-open');
            setTimeout(() => {
                this.overlay.classList.add('hidden');
            }, this.transitionTime);
            return 'close';
        }

        this.navOpen = !this.navOpen;
        this.siteConstraint.classList.add('nav-open');
        this.overlay.classList.remove('hidden');
        return 'open';
    }

    closeNav = () => {
        if(this.navOpen){
            this.toggle();
        }
    }

    bindEvents(){
        this.toggleElement.addEventListener('click', this.toggle);
        this.overlay.addEventListener('click', this.toggle);
        window.addEventListener('resize', this.closeNav);
    }

    unbindEvents(){
        this.toggleElement.removeEventListener('click', this.toggle);
        this.overlay.removeEventListener('click', this.toggle);
        window.removeEventListener('resize', this.closeNav);
    }

    init(){
        this.navOpen = this.siteConstraint.classList.contains('nav-open');
        this.createOverlay();
        this.bindEvents();
    }

}

export default NavDrawer;
import '../scss/main.scss';
import NavDrawer from './NavDrawer.js';
import HoverItems from './HoverItems.js';

(function(){
    window.addEventListener('load', (event) => {
        const navDrawer = new NavDrawer("dui-menu-toggle", "site-constraint", 'main-content');
        const hoverItems = new HoverItems();
    });
})();
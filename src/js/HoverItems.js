export default class HoverItems {
    constructor(){
        this.hoverParentList = document.querySelectorAll('[data-hover="parent"]');
        this.init();
    }

    setChildPosition(){
        const len = this.parentChildArray.length;
        let childPosition;
        for(let i = 0; i < len; i++){
            const child = this.parentChildArray[i][1];
            child.style.opacity = "0";
            child.style.display = "block";
            childPosition = child.getBoundingClientRect();
            if (childPosition.right > window.innerWidth){
                child.style.right = "0px";
                child.style.left = "auto";
            }
            child.style.opacity = "1";
            child.style.display = "none";
        }
    }

    createParentChildMap(){
        const len = this.hoverParentList.length;
        this.parentChildArray = [];
        for (let i = 0; i < len; i++) {
            const hoverChild = this.hoverParentList[i].querySelector('[data-hover="child"]');
            this.parentChildArray.push([this.hoverParentList[i], hoverChild]);
        }
    }

    startHover = (child) => {
        return function hoverEvent() {
            child.style.display = 'block';
        }
    }

    endHover = (child) => {
        return function hoverEvent() {
            child.style.display = 'none';
        }
    }

    bindEvents(){
        this.parentChildArray.forEach(item => {
            item[0].addEventListener('mouseenter', this.startHover(item[1]));
            item[0].addEventListener('mouseleave', this.endHover(item[1]));
        });
    }

    init(){
        this.createParentChildMap();
        this.setChildPosition();
        this.bindEvents();
    }
}
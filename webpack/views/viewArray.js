const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
    new HtmlWebpackPlugin({
        title: 'page 1',
        filename: 'index.html',
        template: './src/views/index.html',
        chunks: ['app']
    })
]